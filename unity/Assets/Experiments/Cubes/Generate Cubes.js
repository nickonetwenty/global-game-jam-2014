﻿#pragma strict

var cube: GameObject;

function Start () {
	for (var i: int = 0; i < 32; i++) {
		for (var j: int = 0; j < 32; j++) {
			GameObject.Instantiate(cube, Vector3(1.5*i, 0, 1.5*j), Quaternion.identity);
		}
	}
}

function Update () {

}