﻿#pragma strict

function Start () {
	var gos : GameObject[];
	
	gos = GameObject.FindObjectsOfType(GameObject);
	
	for (var go: GameObject in gos) {
		if (!go.activeInHierarchy)
			continue;
			
		var rb = go.GetComponent("Rigidbody") as Rigidbody;
		
		if (rb != null)
			Debug.Log(go);
			rb.isKinematic = true;
	}

}

function Update () {

}

function OnTriggerEnter(other : Collider) {
		
		Debug.Log(other);
		
		/*
		if (!other.gameObject.CompareTag("Exists"))
			return;
		*/
			
		var rb = other.gameObject.GetComponent("Rigidbody") as Rigidbody;
			
		if (rb != null)
			Debug.Log(other);
			rb.isKinematic = false;
}