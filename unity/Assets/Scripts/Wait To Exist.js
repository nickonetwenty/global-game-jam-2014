﻿#pragma strict

function Start () {
	rigidbody.isKinematic = true;
}

function Update () {

}

function Perceived() {
	rigidbody.isKinematic = false;
	rigidbody.AddForce(Vector3.up);
	Debug.Log("PERCEIVED");
}

function UnPerceived() {
	rigidbody.isKinematic = true;
	Debug.Log("UNPERCEIVED");
}