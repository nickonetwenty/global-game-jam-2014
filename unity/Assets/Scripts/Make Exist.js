﻿#pragma strict

var radius: float;

function Start () {
}

function Update () {
	var hitColliders = Physics.OverlapSphere(transform.position, radius);
		
	for (var i = 0; i < hitColliders.Length; i++) {
		hitColliders[i].SendMessage("Perceived");
	}
}